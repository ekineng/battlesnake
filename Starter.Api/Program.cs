using System.Collections;
using System.Diagnostics;
using System.Xml.Linq;
using Starter.Api;
using Starter.Api.Model;
using Starter.Api.Requests;
using Starter.Api.Responses;

var builder = WebApplication.CreateBuilder(args);
var app = builder.Build();
app.UseHttpsRedirection();

List<string> direction = direction = new List<string> { "up", "down", "left", "right" };
Dictionary<int, bool> hazardRetry = null;
Dictionary<int, bool> bodyRetry = null;
string mapType = "";

/// <summary>
/// This request will be made periodically to retrieve information about your Battlesnake,
/// including its display options, author, etc.
/// </summary>
app.MapGet("/", () =>
{
    return new InitResponse
    {
        ApiVersion = "1",
        Author = "ekin",
        Color = "#4B0150",
        Head = "default",
        Tail = "default"
    };
});

/// <summary>
/// Your Battlesnake will receive this request when it has been entered into a new game.
/// Every game has a unique ID that can be used to allocate resources or data you may need.
/// Your response to this request will be ignored.
/// </summary>
app.MapPost("/start", (GameStatusRequest gameStatusRequest) =>
{
    mapType = gameStatusRequest.Game.Map;
    Results.Ok();
});

/// <summary>
/// This request will be sent for every turn of the game.
/// Use the information provided to determine how your
/// Battlesnake will move on that turn, either up, down, left, or right.
/// </summary>
app.MapPost("/move", (GameStatusRequest gameStatusRequest) =>
{
    ResetHazardRetry();
    ResetBodyRetry();   

    var direction = new List<string> { "up", "down", "left", "right" };
    int tempCount = 0;
    int nearestFoodIndex = 0;
    int? nearestFoodDistance = null;
    Coordinate nearestFoodCoordinate = new Coordinate(0, 0);
    Snake snake = gameStatusRequest.Board.Snakes.FirstOrDefault();
    Dictionary<int, int> foodsPropertyDict = new Dictionary<int, int>();

    foreach (var food in gameStatusRequest.Board.Food)
    {
        int distanceX = food.X - snake.Head.X;
        int distanceY = food.Y - snake.Head.Y;
        int totalDistance = Math.Abs(distanceX) + Math.Abs(distanceY);
        foodsPropertyDict.Add(tempCount, totalDistance);

        if (nearestFoodDistance == null)
        {
            nearestFoodDistance = totalDistance;
            nearestFoodIndex = tempCount;
            nearestFoodCoordinate = new Coordinate(distanceX, distanceY);
        }
        else if (totalDistance <= nearestFoodDistance)
        {
            nearestFoodDistance = totalDistance;
            nearestFoodIndex = tempCount;
            nearestFoodCoordinate = new Coordinate(distanceX, distanceY);
        }

        tempCount++;
    }

    string moveDirection = "";

    // if coordinate Y is greater than 0 then move up
    if (nearestFoodCoordinate.Y > 0)
    {
        moveDirection = "up";
    }

    // if coordinate Y is lesser than 0 then move down
    if (nearestFoodCoordinate.Y < 0)
    {
        moveDirection = "down";
    }

    // if coordinate X is lesser than 0 then move left
    if (nearestFoodCoordinate.X < 0)
    {
        moveDirection = "left";
    }

    // if coordinate X is greater than 0 then move right
    if (nearestFoodCoordinate.X > 0)
    {
        moveDirection = "right";
    }

    Dictionary<int, int> sortByNearestFoods = new Dictionary<int, int>();

    // sort the foods in ascending order
    foreach (KeyValuePair<int, int> food in foodsPropertyDict.OrderBy(key => key.Value))
    {
        sortByNearestFoods.Add(food.Key, food.Value);
    }

    moveDirection = RevisedTheMoveDirectionToPreventFromBodyCrashAndHazards(snake, gameStatusRequest.Board.Hazards, nearestFoodCoordinate, moveDirection, 0);
    //moveDirection = RevisedTheMoveDirectionToPreventFromOtherSnakesCrash(snake, gameStatusRequest.Board.Snakes, moveDirection);

    // use best first search heuristic algorithm
    moveDirection = BestFirstSearchWithHeuristicAlgorithm(snake, moveDirection, gameStatusRequest);

    return new MoveResponse
    {
        //Move = direction[Random.Shared.Next(direction.Count)],
        Move = moveDirection,
        Shout = "I am moving!"
    };
});

string BestFirstSearchWithHeuristicAlgorithm(Snake snake, string moveDirection, GameStatusRequest gameStatusRequest)
{
    List<Node> tree = new List<Node>();
    //Coordinate nextMoveHead = GetSnakeHeadCoordinateFromMoveDirection(snake, moveDirection);
    var parentCoordinates = GenerateAllPossibleCoordinate(snake.Head, gameStatusRequest.Board.Hazards, gameStatusRequest.You.Body);

    foreach (Coordinate parentCoordinate in parentCoordinates)
    {
        Node node = new Node();
        node.Parent = parentCoordinate;

        foreach (Coordinate coordinate in GenerateAllPossibleCoordinate(node.Parent, gameStatusRequest.Board.Hazards, gameStatusRequest.You.Body))
        {
            node.AddChild(new Node() { Parent = coordinate });
        }

        foreach (var childNode in node.Children)
        {
            foreach (Coordinate coordinate in GenerateAllPossibleCoordinate(childNode.Parent, gameStatusRequest.Board.Hazards, gameStatusRequest.You.Body))
            {
                childNode.AddChild(new Node() { Parent = coordinate });
            }

            foreach (var anotherChildNode in childNode.Children)
            {
                foreach (Coordinate coordinate in GenerateAllPossibleCoordinate(anotherChildNode.Parent, gameStatusRequest.Board.Hazards, gameStatusRequest.You.Body))
                {
                    anotherChildNode.AddChild(new Node() { Parent = coordinate });
                }
            }
        }

        tree.Add(node);
    }

    if (tree.Count <= 2)
    {
        Dictionary<Coordinate, int> totalScore = new Dictionary<Coordinate, int>();
        int tempTraverseCount = 0;
        // traverse the tree and use the best-first search algorithm
        foreach (var treeNode in tree)
        {
            int score = 0;

            score = score + CrashWithHazard(treeNode.Parent, gameStatusRequest.Board.Hazards);
            score = score + CrashWithSnakeBody(treeNode.Parent, gameStatusRequest.You.Body);
            score = score + CrashWithWall(treeNode.Parent);

            foreach (var childNode1 in treeNode.Children)
            {
                score = score + CrashWithHazard(childNode1.Parent, gameStatusRequest.Board.Hazards);
                score = score + CrashWithSnakeBody(childNode1.Parent, gameStatusRequest.You.Body);
                score = score + CrashWithWall(childNode1.Parent);

                foreach (var childNode2 in childNode1.Children)
                {
                    score = score + CrashWithHazard(childNode2.Parent, gameStatusRequest.Board.Hazards);
                    score = score + CrashWithSnakeBody(childNode2.Parent, gameStatusRequest.You.Body);
                    score = score + CrashWithWall(childNode1.Parent);

                    foreach (var childNode3 in childNode1.Children)
                    {
                        score = score + CrashWithHazard(childNode3.Parent, gameStatusRequest.Board.Hazards);
                        score = score + CrashWithSnakeBody(childNode3.Parent, gameStatusRequest.You.Body);
                        score = score + CrashWithWall(childNode3.Parent);
                    }
                }
            }

            totalScore.Add(treeNode.Parent, score);
            tempTraverseCount++;
        }

        KeyValuePair<Coordinate, int> bestFirstDirection = totalScore.OrderBy(key => key.Value).FirstOrDefault();

        if (bestFirstDirection.Key.X - snake.Head.X < 0)
        {
            moveDirection = "left";
        }

        if (bestFirstDirection.Key.X - snake.Head.X > 0)
        {
            moveDirection = "right";
        }

        if (bestFirstDirection.Key.Y - snake.Head.Y < 0)
        {
            moveDirection = "down";
        }

        if (bestFirstDirection.Key.Y - snake.Head.Y > 0)
        {
            moveDirection = "up";
        }

        return moveDirection;
    }

    return moveDirection;
}

int CrashWithHazard(Coordinate nextMoveHead, IEnumerable<Coordinate> hazards)
{
    bool isCrash = false;

    foreach (Coordinate hazard in hazards)
    {
        if (hazard.X == nextMoveHead.X && hazard.Y == nextMoveHead.Y)
        {
            isCrash = true;
            break;
        }
    }

    if (isCrash == false)
    {
        return 0;
    }
    else
    {
        return 1;
    }
}

int CrashWithSnakeBody(Coordinate nextMoveHead, IEnumerable<Coordinate> body)
{
    bool isCrash = false;

    foreach (Coordinate bodyPart in body)
    {
        if (bodyPart.X == nextMoveHead.X && bodyPart.Y == nextMoveHead.Y)
        {
            isCrash = true;
            break;
        }
    }

    if (isCrash == false)
    {
        return 0;
    }
    else
    {
        return 1;
    }
}

int CrashWithWall(Coordinate nextMoveHead)
{
    int score = 0;
    // check whether it crashed the wall
    if (mapType == "standard")
    {
        if (nextMoveHead.X < 0)
        {
            nextMoveHead.X++;
            score++;
        }

        if (nextMoveHead.X > 10)
        {
            nextMoveHead.X--;
            score++;
        }

        if (nextMoveHead.Y < 0)
        {
            nextMoveHead.Y++;
            score++;
        }

        if (nextMoveHead.Y > 10)
        {
            nextMoveHead.Y--;
            score++;
        }
    }

    return score;
}

List<Coordinate> GenerateAllPossibleCoordinate(Coordinate coordinate, IEnumerable<Coordinate> hazards, IEnumerable<Coordinate> bodyParts)
{
    Coordinate tempCoordinate = null;
    List<Coordinate> allPosibleCoordinates = new List<Coordinate>();

    // up
    tempCoordinate = new Coordinate(coordinate.X, coordinate.Y);
    tempCoordinate.Y++;

    if (CrashWithHazard(tempCoordinate, hazards) == 0 && CrashWithSnakeBody(tempCoordinate, bodyParts) == 0 && CrashWithWall(tempCoordinate) == 0)
    {
        allPosibleCoordinates.Add(tempCoordinate);
    }

    // down
    tempCoordinate = new Coordinate(coordinate.X, coordinate.Y);
    tempCoordinate.Y--;

    if (CrashWithHazard(tempCoordinate, hazards) == 0 && CrashWithSnakeBody(tempCoordinate, bodyParts) == 0 && CrashWithWall(tempCoordinate) == 0)
    {
        allPosibleCoordinates.Add(tempCoordinate);
    }

    // left
    tempCoordinate = new Coordinate(coordinate.X, coordinate.Y);
    tempCoordinate.X--;

    if (CrashWithHazard(tempCoordinate, hazards) == 0 && CrashWithSnakeBody(tempCoordinate, bodyParts) == 0 && CrashWithWall(tempCoordinate) == 0)
    {
        allPosibleCoordinates.Add(tempCoordinate);
    }

    // right
    tempCoordinate = new Coordinate(coordinate.X, coordinate.Y);
    tempCoordinate.X++;

    if (CrashWithHazard(tempCoordinate, hazards) == 0 && CrashWithSnakeBody(tempCoordinate, bodyParts) == 0 && CrashWithWall(tempCoordinate) == 0)
    {
        allPosibleCoordinates.Add(tempCoordinate);
    }

    return allPosibleCoordinates;
}

Coordinate GetSnakeHeadCoordinateFromMoveDirection(Snake snake, string moveDirection)
{
    Coordinate nextMoveHead = new Coordinate(snake.Head.X, snake.Head.Y);

    if (moveDirection == "up")
    {
        nextMoveHead.Y++;
    }
    else if (moveDirection == "down")
    {
        nextMoveHead.Y--;
    }
    else if (moveDirection == "left")
    {
        nextMoveHead.X--;
    }
    else if (moveDirection == "right")
    {
        nextMoveHead.X++;
    }

    return nextMoveHead;
}

string RevisedTheMoveDirectionToPreventFromBodyCrashAndHazards(Snake snake, IEnumerable<Coordinate> hazards, Coordinate nearestFoodCoordnate, string moveDirection, int hazardStartIndex, bool isCrashBodyCheckerDone = false)
{
    List<Coordinate> listAllNearestBody = new List<Coordinate>();
    Coordinate nextMoveHead = GetSnakeHeadCoordinateFromMoveDirection(snake, moveDirection);

    int hazardRetryCount = 0;

    foreach (var temp in hazardRetry)
    {
        if (temp.Value == true)
            hazardRetryCount++;
    }

    if (hazardRetryCount == hazardRetry.Count)
    {
        goto breakLoop;
    }

    int bodyRetryCount = 0;

    foreach (var temp in bodyRetry)
    {
        if (temp.Value == true)
            bodyRetryCount++;
    }

    if (bodyRetryCount == bodyRetry.Count)
    {
        goto breakLoop2;
    }

    if (hazardStartIndex >= 0)
    {
        int i = hazardStartIndex;

        for (i = 0; i < hazards.Count(); i++)
        {
            if (hazards.ElementAt(i).X == nextMoveHead.X && hazards.ElementAt(i).Y == nextMoveHead.Y)
            {
                hazardRetry[direction.IndexOf(moveDirection)] = true;

                foreach (var key in hazardRetry.Keys)
                {
                    if (hazardRetry[key] == false)
                    {
                        if (key == 0)
                        {
                            moveDirection = "up";
                        }
                        else if (key == 1)
                        {
                            moveDirection = "down";
                        }
                        else if (key == 2)
                        {
                            moveDirection = "left";
                        }
                        else if (key == 3)
                        {
                            moveDirection = "right";
                        }

                        break;
                    }
                }

                return RevisedTheMoveDirectionToPreventFromBodyCrashAndHazards(snake, hazards, nearestFoodCoordnate, moveDirection, i);
            }
        }
    }

breakLoop:

    if (!isCrashBodyCheckerDone)
    {
        bool isChangeDirection = false;

        for (int i = 0; i < snake.Body.Count(); i++)
        {
            if (snake.Body.ElementAt(i).X == nextMoveHead.X && snake.Body.ElementAt(i).Y == nextMoveHead.Y)
            {
                string concat = "";

                foreach (var bodyCoordinates in snake.Body)
                {
                    concat = concat + $"({bodyCoordinates.X},{bodyCoordinates.Y}) ";
                }

                bodyRetry[direction.IndexOf(moveDirection)] = true;

                foreach (var key in bodyRetry.Keys)
                {
                    if (bodyRetry[key] == false)
                    {
                        if (key == 0)
                        {
                            moveDirection = "up";
                        }
                        else if (key == 1)
                        {
                            moveDirection = "down";
                        }
                        else if (key == 2)
                        {
                            moveDirection = "left";
                        }
                        else if (key == 3)
                        {
                            moveDirection = "right";
                        }

                        break;
                    }
                }

                isChangeDirection = true;
                break;
            }
        }

        if (isChangeDirection == true)
        {
            return RevisedTheMoveDirectionToPreventFromBodyCrashAndHazards(snake, hazards, nearestFoodCoordnate, moveDirection, -1);
        }

        isCrashBodyCheckerDone = true;
    }

 breakLoop2:

    nextMoveHead = GetSnakeHeadCoordinateFromMoveDirection(snake, moveDirection);

    if (CrashWithWall(nextMoveHead) > 0)
    {
        ResetBodyRetry();
        Random random = new Random();
        moveDirection = direction[random.Next(direction.Count)];

        Debug.WriteLine("###" + moveDirection);

        return RevisedTheMoveDirectionToPreventFromBodyCrashAndHazards(snake, hazards, nearestFoodCoordnate, moveDirection, -1);
    }

    return moveDirection;
}

void ResetHazardRetry()
{
    hazardRetry = new Dictionary<int, bool>
    {
        { 0, false },
        { 1, false },
        { 2, false },
        { 3, false }
    };
}

void ResetBodyRetry()
{
    bodyRetry = new Dictionary<int, bool>
    {
        { 0, false },
        { 1, false },
        { 2, false },
        { 3, false }
    };
}

/// <summary>
/// Your Battlesnake will receive this request whenever a game it was playing has ended.
/// Use it to learn how your Battlesnake won or lost and deallocated any server-side resources.
/// Your response to this request will be ignored.
/// </summary>
app.MapPost("/end", (GameStatusRequest gameStatusRequest) =>
{
    Debug.WriteLine("-------- END --------");

    string concat = "";
    Debug.WriteLine("Body:");

    foreach (var bodyCoordinates in gameStatusRequest.You.Body)
    {
        concat = concat + $"({bodyCoordinates.X},{bodyCoordinates.Y}) ";
    }

    Debug.WriteLine(concat);

    Results.Ok();
});

app.Run();


//string RevisedTheMoveDirectionToPreventFromOtherSnakesCrash(Snake snake, IEnumerable<Snake> otherSnakes, string moveDirection)
//{
//    List<Coordinate> listAllNearestBody = new List<Coordinate>();

//    if (moveDirection == "up")
//    {   
//        snake.Head.Y++;
//    }
//    else if (moveDirection == "down")
//    {
//        snake.Head.Y--;
//    }
//    else if (moveDirection == "left")
//    {
//        snake.Head.X--;
//    }
//    else if (moveDirection == "right")
//    {
//        snake.Head.X++;
//    }

//    foreach (Snake otherSnake in otherSnakes)
//    {
//        foreach (var otherSnakeBody in otherSnake.Body)
//        {
//            if (otherSnakeBody == snake.Head)
//            {
//                Random random = new Random();
//                moveDirection = direction[random.Next(direction.Count)];
//                break;
//            }
//        }
//    }

//    return RevisedTheMoveDirectionToPreventFromOtherSnakesCrash(snake, otherSnakes, moveDirection);
//}