﻿namespace Starter.Api.Model
{
    public class Node
    {
        //public int Level { get; set; }
        public Coordinate Parent { get; set; }
        public List<Node> Children = new List<Node>();

        public void AddChild(Node childNode)
        {
            this.Children.Add(childNode);
        }
    }
}
